package com.example.kali.movies;

import android.media.Image;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.squareup.picasso.Picasso;

public class ShowDetails  extends AppCompatActivity {
    Show show;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_details);
        show = new Show();
        Bundle bundle = getIntent().getExtras();
        if(bundle != null)
            show = bundle.getParcelable("show");
        TextView title = (TextView) findViewById(R.id.textViewTitle);
        TextView overview = (TextView) findViewById(R.id.textViewOverview);
        ImageView image = (ImageView) findViewById(R.id.imagePoster);
        title.setText(show.getTitle());
        overview.setText(show.getBasicInfo());
        String url = show.getImage().toString();
        Picasso.get().load(url).into(image);
    }

}
