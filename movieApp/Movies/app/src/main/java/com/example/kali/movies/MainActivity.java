package com.example.kali.movies;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.PersistableBundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.ToggleButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements FragmentPopular.OnDataPass, FragmentSearch.OnDataPass {
    private static FragmentManager fm;
    FragmentPopular fragmentPopular;
    FragmentSearch fragmentSearch;
    Boolean Movie;
    SearchView searchView;
    String query;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Movie = false;
        fm = getFragmentManager();
        FrameLayout fragment = (FrameLayout) findViewById(R.id.placeF1);
        fragmentPopular = (FragmentPopular)fm.findFragmentById(R.id.placeF1);
        if(fragmentPopular == null) {
            fragmentPopular = new FragmentPopular();
            fm.beginTransaction().replace(R.id.placeF1, fragmentPopular).addToBackStack(null).commit();
        }
        searchView = (SearchView)findViewById(R.id.searchBar);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if(s.length() >= 3) {
                    query = s;
                    fragmentSearch = new FragmentSearch();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("movieOrTVShow", Movie);
                    bundle.putString("query", s);
                    fragmentSearch.setArguments(bundle);
                    fm.beginTransaction().replace(R.id.placeF1, fragmentSearch).addToBackStack(null).commit();
                } else {
                    fm.beginTransaction().replace(R.id.placeF1, fragmentPopular).addToBackStack(null).commit();
                }
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                searchView.setQuery("", false);
                Bundle bundle = new Bundle();
                bundle.putBoolean("movieOrTVShow", Movie);
                fragmentPopular = new FragmentPopular();
                fragmentPopular.setArguments(bundle);
                fm.beginTransaction().replace(R.id.placeF1, fragmentPopular).addToBackStack(null).commit();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void movieOrTVShow(Boolean movie) {
        Movie = movie;
    }

    @Override
    public void showDetails(Show show) {
        Intent intent = new Intent(MainActivity.this, ShowDetails.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("show", show);
        intent.putExtras(bundle);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityIfNeeded(intent,0);
    }
}
