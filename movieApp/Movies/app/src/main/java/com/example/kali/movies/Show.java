package com.example.kali.movies;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.URL;

public class Show implements Parcelable{
    String title;
    String basicInfo;
    URL image;
    public Show() {}
    public Show(String title, String basicInfo, URL image) {
        this.title = title;
        this.basicInfo = basicInfo;
        this.image = image;

    }
    public Show(Parcel parcel){
        this.title = parcel.readString();
        this.basicInfo = parcel.readString();
        this.image = (URL) parcel.readSerializable();

    }
    public String getTitle() {
        return title;
    }

    public String getBasicInfo() {
        return basicInfo;
    }

    public URL getImage() {
        return image;
    }

    public  void setTitle(String title) {
        this.title = title;
    }

    public void setBasicInfo(String basicInfo) {
        this.basicInfo = basicInfo;
    }

    public void setImage(URL image) {

        this.image = image;
    }
    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.title);
        parcel.writeString(this.basicInfo);
        parcel.writeSerializable(this.image);

    }
    public static Parcelable.Creator<Show> CREATOR = new Parcelable.Creator<Show>() {
        @Override
        public Show createFromParcel(Parcel parcel) {
            return new Show(parcel);
        }

        @Override
        public Show[] newArray(int i) {
            return new Show[i];
        }
    };
}
