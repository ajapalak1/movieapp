package com.example.kali.movies;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class FragmentSearch extends Fragment implements SearchMovies.ISearchMoviesDone, SearchTVShows.ISearchTVShowsDone {
    ArrayList<Show> showsList;
    ShowArrayAdapter adapter;
    Boolean Movie;
    OnDataPass dataPasser;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPasser = (FragmentSearch.OnDataPass) context;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final ListView list = (ListView) getActivity().findViewById(R.id.listSearch);
        showsList = new ArrayList<Show>();
        ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if(ab != null){
            ab.setDisplayHomeAsUpEnabled(true);
        }

        adapter = new ShowArrayAdapter(getActivity(), R.layout.custom_list_element, showsList, getResources());
        list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        Movie = false;
        Movie = getArguments().getBoolean("movieOrTVShow");
        String query = getArguments().getString("query");
        if(Movie)
            new SearchMovies((SearchMovies.ISearchMoviesDone) FragmentSearch.this).execute(query);
        else
            new SearchTVShows((SearchTVShows.ISearchTVShowsDone) FragmentSearch.this).execute(query);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                dataPasser.showDetails((Show)list.getItemAtPosition(i));
            }
        });
    }

    @Override
    public void ongetMoviesDone(ArrayList<Show> movies) {
        showsList.clear();
        showsList.addAll(movies);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void ongetTVShowsDone(ArrayList<Show> shows) {
        showsList.clear();
        showsList.addAll(shows);
        adapter.notifyDataSetChanged();

    }

    public interface OnDataPass {
        public void showDetails(Show show);
    }
}
