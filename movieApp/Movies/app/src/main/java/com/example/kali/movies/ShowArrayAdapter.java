package com.example.kali.movies;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;



import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class ShowArrayAdapter extends ArrayAdapter<Show> {
    onButtonClick onClick;
    int resource;
    public Resources res;
    public ShowArrayAdapter(@NonNull Context context, int _resource, List<Show> items, Resources resLocal) {
        super(context, _resource, items);
        resource = _resource;
        res = resLocal;

    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent ){
        LinearLayout newView;
        if(convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout)convertView;
        }
        final Show classInstance = getItem(position);
        ImageView image = (ImageView)newView.findViewById(R.id.eImage);
        TextView title = (TextView)newView.findViewById(R.id.eTitle);


        title.setText(classInstance.getTitle());

        String url = classInstance.getImage().toString();
        Picasso.get().load(url).into(image);
        return newView;
    }
    public interface onButtonClick{
        public void onClick(int position);
    }
}
