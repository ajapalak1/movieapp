package com.example.kali.movies;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ToggleButton;

import java.util.ArrayList;

public class FragmentPopular extends Fragment implements GetTVShows.IGetTVShowsDone , GetMovies.IGetMoviesDone {
    ArrayList<Show> showsList;
    ShowArrayAdapter adapter;
    Boolean Movie;
    OnDataPass dataPasser;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        return inflater.inflate(R.layout.fragment_popular, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPasser = (OnDataPass) context;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if(ab != null){
            ab.setDisplayHomeAsUpEnabled(false);
        }
        final ToggleButton movies = (ToggleButton) getActivity().findViewById(R.id.toggleMovies);
        final ToggleButton shows = (ToggleButton) getActivity().findViewById(R.id.toggleTVShow);
        final ListView list = (ListView) getActivity().findViewById(R.id.list);
        showsList = new ArrayList<Show>();
        adapter = new ShowArrayAdapter(getActivity(), R.layout.custom_list_element, showsList, getResources());
        list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        Bundle argumetns = getArguments();
        if(argumetns == null)
            Movie = false;
        else
            Movie = getArguments().getBoolean("movieOrTVShow");
        dataPasser.movieOrTVShow(Movie);
        if(!Movie) {
            new GetTVShows((GetTVShows.IGetTVShowsDone) FragmentPopular.this).execute();
            movies.setChecked(false);
            shows.setChecked(true);
        }
        else {
            new GetMovies((GetMovies.IGetMoviesDone) FragmentPopular.this).execute();
            movies.setChecked(true);
            shows.setChecked(false);
        }
        shows.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shows.isChecked()) {
                    movies.setChecked(false);
                    Movie = false;
                    dataPasser.movieOrTVShow(Movie);
                    new GetTVShows((GetTVShows.IGetTVShowsDone) FragmentPopular.this).execute();
                }
                else
                    shows.setChecked(true);
            }
        });
        movies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(movies.isChecked()) {
                    shows.setChecked(false);
                    Movie = true;
                    dataPasser.movieOrTVShow(Movie);
                    new GetMovies((GetMovies.IGetMoviesDone) FragmentPopular.this).execute();

                }
                else
                    movies.setChecked(true);
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                dataPasser.showDetails((Show)list.getItemAtPosition(i));
            }
        });
    }

    @Override
    public void ongetTVShowsDone(ArrayList<Show> shows) {
        showsList.clear();
        showsList.addAll(shows);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void ongetMoviesDone(ArrayList<Show> movies) {
        showsList.clear();
        showsList.addAll(movies);
        adapter.notifyDataSetChanged();
    }

    public interface OnDataPass {
        public void movieOrTVShow(Boolean movie);
        public void showDetails(Show show);
    }
}
