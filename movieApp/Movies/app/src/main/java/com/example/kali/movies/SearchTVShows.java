package com.example.kali.movies;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class SearchTVShows extends AsyncTask<String, Integer, Void> {

    public interface ISearchTVShowsDone {
        public void ongetTVShowsDone(ArrayList<Show> shows);
    }

    ArrayList<Show> getTVShows;
    private ISearchTVShowsDone caller;

    public SearchTVShows(ISearchTVShowsDone p) {
        getTVShows = new ArrayList<Show>();
        caller = p;
    }
    @Override
    protected Void doInBackground(String... strings) {
        String query = null;
        String[] queries = null;
        try {
            query = URLEncoder.encode(strings[0], "utf-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url1 = "https://api.themoviedb.org/3/search/tv?api_key=f93d14cf23a6f0188dc9b021d784f24d&query=" + query;
        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String results = convertStreamToString(in);
            JSONObject jo = new JSONObject(results);
            JSONArray movies = jo.getJSONArray("results");
            ArrayList<JSONObject> popularMovies = new ArrayList<JSONObject>();
            for (int i = 0; i < movies.length(); i++) {
                popularMovies.add(movies.getJSONObject(i));
            }

            for (int i = 0; i < popularMovies.size(); i++) {
                JSONObject movie = popularMovies.get(i);
                String title = movie.getString("original_name");
                URL image = new URL("https://image.tmdb.org/t/p/w500/" + movie.getString("poster_path"));
                String basicInfo = movie.getString("overview");
                getTVShows.add(new Show(title, basicInfo, image));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
    protected void onPostExecute(Void avoid) {
        super.onPostExecute(avoid);
        caller.ongetTVShowsDone(getTVShows);
    }

    private String convertStreamToString(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {

        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}